﻿using MatchService.Data;
using MatchService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Services
{
    public class LikeItemData
    {
        private readonly MyDBContext _context;
        private readonly MatchItemData matchItemService;
        public LikeItemData(MyDBContext context)
        {
            _context = context;
            matchItemService = new MatchItemData(_context);
        }

        public List<LikeItem> findByUserId(string id)
        {
            try
            {
                var like = _context.LikeItems
                    .Where(l => l.liked_user_id == id)
                    .ToList();
                return like;
            }
            catch (Exception)
            { 
                return null;
            }
        }

        public LikeItem addLikeItem(string id, LikeItem likeItemToAdd)
        {
            try
            { 
                var likeItem = new LikeItem
                {
                    user_id = id,
                    liked_user_id = likeItemToAdd.liked_user_id,
                    status = likeItemToAdd.status
                };
                var res = _context.LikeItems.Add(likeItem);


                // if liked user have already liked user => add match item
                var mutualLike = _context.LikeItems.Any(l => l.liked_user_id == likeItem.user_id && l.user_id == likeItem.liked_user_id);
                if (!matchItemService.MatchItemExists(likeItem.user_id, likeItem.liked_user_id) && mutualLike)
                {
                    var match = new MatchItem
                    {
                        fisrt_user_id = likeItem.liked_user_id,
                        second_user_id = likeItem.user_id,
                        match_date = DateTime.Today.ToString("dd/MM/yyyy")
                    };
                    _context.MatchItems.Add(match);
                }
                // end if
                _context.SaveChanges();
                return res.Entity;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool LikeItemExists(string user_id, string liked_user_id)
        {
            return _context.LikeItems.Any(e => e.user_id == user_id && e.liked_user_id == liked_user_id);
        }

    }
}
