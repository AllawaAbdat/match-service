﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MatchService.Data;
using MatchService.Models;
using MatchService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MatchService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LikeController : ControllerBase
    {
        private readonly LikeItemData _likeItemService;
        public LikeController(MyDBContext context)
        {
            _likeItemService = new LikeItemData(context);
        }

        /// <summary>
        /// List All like of one user
        /// </summary>
        [HttpGet("{user_id}")]
        [ProducesResponseType(typeof(List<LikeItem>), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<LikeItem>> GetUserItem(string user_id)
        {
            var res = _likeItemService.findByUserId(user_id);
            if (res == null)
                return BadRequest("bad input parameter");
            return Ok(res);
        }

        /// <summary>
        /// Add a new like
        /// </summary>
        [HttpPost("{user_id}")]
        [ProducesResponseType(typeof(CreatedResult), 201)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(ContentResult), 409)]
        public ActionResult<LikeItem> PostUserItem(string user_id, [FromBody]LikeItem likeItem)
        {
            if (_likeItemService.LikeItemExists(user_id,likeItem.liked_user_id))
                return Content(HttpStatusCode.Conflict, "an existing item already exists");
            var res = _likeItemService.addLikeItem(user_id, likeItem);
            if (res == null)
                return BadRequest("invalid input, object invalid");
            return Created(res.id, res);
        }

        private ActionResult<LikeItem> Content(HttpStatusCode conflict, string v)
        {
            return Content(conflict, v);
        }
    }
}