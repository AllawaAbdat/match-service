﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MatchService.Data;
using MatchService.Models;
using MatchService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MatchService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RecommandationController : ControllerBase
    {
        private readonly CarateristicItemData _caracteristicItemService;
        public RecommandationController(MyDBContext context)
        {
            _caracteristicItemService = new CarateristicItemData(context);
        }

        /// <summary>
        /// Get list of possible recommandation
        /// </summary>
        [HttpPost()]
        [ProducesResponseType(typeof(List<CaracteristicItem>), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<CaracteristicItem>> GetMatchs([FromBody] ParameterItem parameterItem)
        {
            var res = _caracteristicItemService.findByParameter(parameterItem);
            if (res == null)
                return BadRequest("bad input parameter");
            return Ok(res);
        }

        /// <summary>
        /// List caracteristic of the user
        /// </summary>
        [HttpGet("{user_id}/caracteristic")]
        [ProducesResponseType(typeof(CaracteristicItem), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<CaracteristicItem>> GetCaracteristic(string user_id)
        {
            var res = _caracteristicItemService.findByUserId(user_id);
            if (res == null)
                return BadRequest("bad input parameter");
            return Ok(res);
        }

        /// <summary>
        /// Add a user caracteristic
        /// </summary>
        [HttpPost("{user_id}/caracteristic")]
        [ProducesResponseType(typeof(CreatedResult), 201)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(ContentResult), 409)]
        public ActionResult<CaracteristicItem> AddCaracteristic(string user_id, [FromBody] CaracteristicItem caracteristicItem)
        {
            if (_caracteristicItemService.findByUserId(user_id) != null)
                return Content(HttpStatusCode.Conflict, "an existing item already exists");

            if (user_id != caracteristicItem.user_id)
                return BadRequest("invalid input, object invalid");

            var res = _caracteristicItemService.addCarasteristicItem(caracteristicItem);
            if (res == null)
                return BadRequest("invalid input, object invalid");

            return Created(res.id, res);

        }

        /// <summary>
        /// Update a user caracteristic
        /// </summary>
        [HttpPut("{user_id}/caracteristic")]
        [ProducesResponseType(typeof(CaracteristicItem), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<CaracteristicItem> UpdateCaracteristic(string user_id, [FromBody] CaracteristicItem caracteristicItem)
        {
      
            if (_caracteristicItemService.findByUserId(user_id) == null || user_id != caracteristicItem.user_id)
                return BadRequest("invalid input, object invalid");

            var res = _caracteristicItemService.updateCarasteristicItem(caracteristicItem);
            if (res == null)
                return BadRequest("invalid input, object invalid");

            return Ok(res);

        }
        private ActionResult<CaracteristicItem> Content(HttpStatusCode conflict, string v)
        {
            return Content(conflict, v);
        }

    }
}