﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Models
{
    public class ParameterItem
    {
        [Required]
        [Range(18,100)]
        public int age_max { get; set; }
        [Required]
        [Range(18, 100)]
        public int age_min { get; set; }
        // true: female ||false : male
        [Required]
        public bool gender { get; set; }
    }
}
