﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MatchService.Data;
using MatchService.Models;
using MatchService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MatchService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly MatchItemData _matchItemService;
        public MatchController(MyDBContext context)
        {
            _matchItemService = new MatchItemData(context);
        }

        /// <summary>
        /// Get list of possible match
        /// </summary>
        [HttpGet()]
        [ProducesResponseType(typeof(List<MatchItem>), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<MatchItem>> GetMatchs()
        {
            //TODO: Add Pagination
            var res = _matchItemService.getMatchItems();
            if (res == null)
                return BadRequest("bad input parameter");
            return Ok(res);
        }

        /// <summary>
        /// Add a new match
        /// </summary>
        [HttpPost()]
        [ProducesResponseType(typeof(OkResult), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<MatchItem>> AddMatch([FromBody] MatchItem matchItem)
        {
            if (_matchItemService.MatchItemExists(matchItem.fisrt_user_id, matchItem.second_user_id))
                return BadRequest("match already exists");

            var res = _matchItemService.addMatchItem(matchItem);
            if (res == null)
                return BadRequest("bad input parameter");

            return Ok("new match has been added");
        }

        /// <summary>
        /// List all match of one user
        /// </summary>
        [HttpGet("{user_id}")]
        [ProducesResponseType(typeof(List<MatchItem>), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<MatchItem>> GetMatchsByUserId(string user_id)
        {
            var res = _matchItemService.getMatchItemsByUserId(user_id);
            if (res == null)
                return BadRequest("bad input parameter");
            return Ok(res);
        }
    }
}