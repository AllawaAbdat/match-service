#!/bin/sh

set -e

  docker build -t my-match-service .
  docker run -d --name my-running-match-service my-match-service
  if docker ps | grep -q my-running-match-service; then
        echo Docker my-running-match-service found
    else
        echo Docker my-running-match-service not found
        exit
  fi