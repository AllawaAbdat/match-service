﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Models
{
    public class LikeItem
    {
        public string id { get; set; }
        [Required]
        public string user_id { get; set; }
        [Required]
        public string liked_user_id { get; set; }
        [Required]
        public bool status { get; set; }
    }
}
