﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Models
{
    public class PhotoItem
    {
        public string id { get; set; }
        public string url { get; set; }
        public string format { get; set; }
        public int width { get; set; }
        public int height { get; set; }
      
    }
}
