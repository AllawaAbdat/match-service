﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Models
{
    public class CaracteristicItem
    {
        [Key]
        public string id { get; set; }

        [Required]
        public string user_id { get; set; }

        [Required]
        public string bio { get; set; }

        [Required]
        [Range(18, 100)]
        public int age { get; set; }

        [Required]
        public bool gender { get; set; }
  
        public string? photo_id { get; set; }

        [ForeignKey("photo_id")]
        public virtual PhotoItem photo { get; set; }
    }
}
