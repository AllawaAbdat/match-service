﻿using MatchService.Data;
using MatchService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Services
{
    public class CarateristicItemData
    {
        private readonly MyDBContext _context;
        public CarateristicItemData(MyDBContext context)
        {
            _context = context;
        }

        public List<CaracteristicItem> findByParameter( ParameterItem parameterItem)
        {
            try
            {
                var caracteristicItem = _context.CaracteristicItems
                    .Where(l => 
                        (l.age >= parameterItem.age_min) && 
                        (l.age <= parameterItem.age_max) &&
                        (l.gender == parameterItem.gender))
                    
                    
                    .ToList();

                foreach (var item in caracteristicItem)
                {
                    item.photo = _context.PhotoItems.Find(item.photo_id);
                }

    
                return caracteristicItem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public CaracteristicItem findByUserId(string user_id)
        {
            try
            {
                var caracteristicItem = _context.CaracteristicItems
                    .AsNoTracking()
                    .Where(l => l.user_id == user_id)
                    .FirstOrDefault();
                caracteristicItem.photo = _context.PhotoItems.Find(caracteristicItem.photo_id);
                return caracteristicItem;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public CaracteristicItem addCarasteristicItem(CaracteristicItem caracteristicItemToAdd)
        {
            try
            {
                var photo = new PhotoItem
                {
                    format = caracteristicItemToAdd.photo.format,
                    url = caracteristicItemToAdd.photo.url,
                    width = caracteristicItemToAdd.photo.width,
                    height = caracteristicItemToAdd.photo.height
                };
                _context.PhotoItems.Add(photo);

                var caracteristicItem = new CaracteristicItem
                {
                    user_id = caracteristicItemToAdd.user_id,
                    bio = caracteristicItemToAdd.bio,
                    age = caracteristicItemToAdd.age,
                    gender = caracteristicItemToAdd.gender,
                    photo_id = photo.id
                };
               
                _context.CaracteristicItems.Add(caracteristicItem);
                _context.SaveChanges();
                return caracteristicItem;

            }
            catch (Exception)
            {
                return null;
            }
        }
        public CaracteristicItem updateCarasteristicItem(CaracteristicItem caracteristicItemToUpdate)
        {
            try
            {
                _context.PhotoItems.Update(caracteristicItemToUpdate.photo);
                var res = _context.CaracteristicItems.Update(caracteristicItemToUpdate);
                _context.SaveChanges();
                return res.Entity;

            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
