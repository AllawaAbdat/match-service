Hi everyone !

On this repository you will be able to find a Docker image which allows you to run the Match Service for the CLO5 Project.

The tree of this Service is the following :

Dockerfile => File used to build a .NET docker image with the "Match-Service" API
Folder "CI" => Folder with 2 scripts (match_service_run.sh & match_service_deploy.sh). Used for the .gitlab-ci.yml
.gitlab-ci.yml => File which allows the Pipeline to be run on Gitlab. (With differents Jobs)

The following list is the User-Service API files :
-Controllers /
-Data /
-Migrations / 
-Models / 
-Services / 
-Migrations / 
-Properties / 
-bin / 
-Program.cs / 
-Startup.cs / 
-MatchService.csproj / 
-MatchService.csproj.user / 
-MatchService.sln
