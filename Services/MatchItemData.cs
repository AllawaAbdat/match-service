﻿using MatchService.Data;
using MatchService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Services
{
    public class MatchItemData
    {
        private readonly MyDBContext _context;
        public MatchItemData(MyDBContext context)
        {
            _context = context;
        }

        public List<MatchItem> getMatchItems()
        {
            try
            {
                //TODO: add pagination
                var res = _context.MatchItems.ToList();
                return res;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public MatchItem addMatchItem(MatchItem matchToAdd)
        {
            try
            {
                var match = new MatchItem
                {
                    fisrt_user_id = matchToAdd.fisrt_user_id,
                    second_user_id = matchToAdd.second_user_id,
                    match_date = DateTime.Today.ToString("dd/MM/yyyy")
                };
                _context.MatchItems.Add(match);
                _context.SaveChanges();
                return match;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<MatchItem> getMatchItemsByUserId(string id)
        {
            try
            {
                  var res = _context.MatchItems
                    .Where(u => u.second_user_id == id || u.fisrt_user_id == id)
                    .ToList();
                return res;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool MatchItemExists(string first_user_id, string second_user_id)
        {
            return _context.MatchItems.Any(e => 
                    (e.fisrt_user_id == first_user_id || e.fisrt_user_id == second_user_id)&&
                    (e.second_user_id == first_user_id || e.second_user_id == second_user_id));
        }
    }
}
