﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MatchService.Migrations
{
    public partial class BDD1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LikeItems",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    user_id = table.Column<string>(nullable: false),
                    liked_user_id = table.Column<string>(nullable: false),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LikeItems", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "MatchItems",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    fisrt_user_id = table.Column<string>(nullable: false),
                    second_user_id = table.Column<string>(nullable: false),
                    match_date = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchItems", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "PhotoItems",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    url = table.Column<string>(nullable: true),
                    format = table.Column<string>(nullable: true),
                    width = table.Column<int>(nullable: false),
                    height = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoItems", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CaracteristicItems",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    user_id = table.Column<string>(nullable: false),
                    bio = table.Column<string>(nullable: false),
                    age = table.Column<int>(nullable: false),
                    gender = table.Column<bool>(nullable: false),
                    photo_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaracteristicItems", x => x.id);
                    table.ForeignKey(
                        name: "FK_CaracteristicItems_PhotoItems_photo_id",
                        column: x => x.photo_id,
                        principalTable: "PhotoItems",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaracteristicItems_photo_id",
                table: "CaracteristicItems",
                column: "photo_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CaracteristicItems");

            migrationBuilder.DropTable(
                name: "LikeItems");

            migrationBuilder.DropTable(
                name: "MatchItems");

            migrationBuilder.DropTable(
                name: "PhotoItems");
        }
    }
}
