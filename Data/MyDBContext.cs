﻿using MatchService.Models;
using Microsoft.EntityFrameworkCore;

namespace MatchService.Data
{
    public class MyDBContext : DbContext
    {

        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options)
        {}

        public DbSet<LikeItem> LikeItems { get; set; }
        public DbSet<CaracteristicItem> CaracteristicItems { get; set; }
        public DbSet<MatchItem> MatchItems { get; set; }
        public DbSet<PhotoItem> PhotoItems { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //Auto - Incremented Id
            builder.Entity<LikeItem>()
                .Property(s => s.id)
                .ValueGeneratedOnAdd();

            builder.Entity<CaracteristicItem>()
                .Property(s => s.id)
                .ValueGeneratedOnAdd();

            builder.Entity<MatchItem>()
                .Property(s => s.id)
                .ValueGeneratedOnAdd();

            builder.Entity<PhotoItem>()
              .Property(s => s.id)
              .ValueGeneratedOnAdd();

           
        }
    }
}
