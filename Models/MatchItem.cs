﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MatchService.Models
{
    public class MatchItem
    {
        public string id { get; set; }
        [Required]
        public string fisrt_user_id { get; set; }
        [Required]
        public string second_user_id { get; set; }
        [Required]
        public string match_date { get; set; }
    }
}
